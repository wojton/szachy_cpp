#ifndef CONSTANTS
#define CONSTANTS

#include <iostream>
using namespace std;

//----------Klasa zawierajaca stale----------//
class Constants {
public:
    //------frames----------//
    const static int startFrame = 0;

    //------scenes----------//
    const static int startScene = 0;
    const static int whitePromoScene = 1;
    const static int blackPromoScene = 2;

    //------odp false z promocji----------//
    const static int clickOutOfPromoBox = 100;
};

#endif // CONSTANTS
