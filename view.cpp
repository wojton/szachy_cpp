#include <iostream>
#include "view.h"

using namespace std;

//---------------------------------------------------------------//
//------------------------------WIDOK----------------------------//
//---------------------------------------------------------------//


//-------------------WIDOK Glowny-------------------//

View::View(int offX, int offY, int mapWidth, int mapHeight, int fieldWidth, int fieldHeight, SDL_Surface *surface) {
    mapOffsetX = offX;
    mapOffsetY = offY;
    mapW = mapWidth;
    mapH = mapHeight;
    fieldH = fieldHeight;
    fieldW = fieldWidth;
    mapSurface = surface;
    grabCoursor = IMG_Load("img/grab2.png");
    cout << "tworze obiekt widoku: " << mapW << endl;
}

View::~View() {
    cout << "usuwam widok" << endl;
}

void View::renderView(SDL_Rect *rect, SDL_Surface *ekran, PawnArrangement ***pawns) {
    //----------------rysowanie mapy--------------------//
    rect->x = 0;
    rect->y = 0;
    SDL_BlitSurface(mapSurface, NULL, ekran, rect); //ustawienie pierwszego prostokata i wrzucenie go do okna

    //--------------rysowanie pionkow-------------------//
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (pawns[i][j]) {
                if (mouseButtonDownFlag && i == row_clicked && j == col_clicked) {
                    rect->x = mouseMotionX - (fieldW / 2);
                    rect->y = mouseMotionY - (fieldH / 2);
//                    SDL_BlitSurface(grabCoursor, NULL, ekran, rect);
                } else {
                    rect->x = mapOffsetX + pawns[i][j]->col * fieldW;
                    rect->y = mapOffsetY + pawns[i][j]->row * fieldH;
                }
                SDL_BlitSurface(pawns[i][j]->pawnSurface, NULL, ekran, rect);
            }
        }
    }
}

void View::freeSurface(PawnArrangement ***pawns) {
    SDL_FreeSurface(mapSurface); //usuniecie obszaru mapy
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (pawns[i][j]) {
                SDL_FreeSurface(pawns[i][j]->pawnSurface);
            }
        }
    }
}


void View::mouseBtnDown(int pressedX, int pressedY) {
    if ((pressedX >= mapOffsetX) && (pressedX <= mapW + mapOffsetX) && (pressedY >= mapOffsetY) && (pressedY <= mapH + mapOffsetY)) {
        int row = (pressedY - mapOffsetY) / fieldW;
        int col = (pressedX - mapOffsetX) / fieldH;
        row_clicked = row;
        col_clicked = col;
        mouseButtonDownFlag = true; //ustawienie flagi na true
    }
}


PawnMove View::mouseBtnUp(int releasedX, int releasedY) {
    int row, col;
    if ((releasedX >= mapOffsetX) && (releasedX <= mapW + mapOffsetX) && (releasedY >= mapOffsetY) && (releasedY <= mapH + mapOffsetY)) {
        row = (releasedY - mapOffsetY) / fieldH;
        col = (releasedX - mapOffsetX) / fieldW;
    } else {
        row = row_clicked;
        col = col_clicked;
    }
    mouseButtonDownFlag = false; //ustawienie flagi na false
    PawnMove pawnMove(row_clicked, col_clicked, row, col);
//    cout << "[" << row_clicked << "," << col_clicked << "]" << " -> " << "[" << row << "," << col << "]" << endl;
    return pawnMove;
}


void View::setMouseMotionCoord(int mouseX, int mouseY) {
    if (mouseButtonDownFlag) {
        if (mouseX < mapOffsetX) {
            mouseMotionX = mapOffsetX;
        } else if ((mouseX >= mapOffsetX) && (mouseX <= (mapOffsetX + mapW))){
            mouseMotionX = mouseX;
        } else {
            mouseMotionX = mapOffsetX + mapW;
        }

        if (mouseY < mapOffsetY) {
            mouseMotionY = mapOffsetY;
        } else if ((mouseY >= mapOffsetY) && (mouseY <= (mapOffsetY + mapH))){
            mouseMotionY = mouseY;
        } else {
            mouseMotionY = mapOffsetY + mapH;
        }
    }
}


//-------------------WIDOK Promocji-------------------//
Promotion::Promotion(int promoOffX, int promoOffY, int fWidth, int fHeight) {
    promoOffsetX = promoOffX;
    promoOffsetY = promoOffY;
    fieldW = fWidth;
    fieldH = fHeight;
    promoSurface = IMG_Load("img/promocja.png");
}

Promotion::~Promotion() {
    cout << "Usuwam obiekt promocji" << endl;
}

void Promotion::renderView(SDL_Rect *rect, SDL_Surface *ekran, PawnArrangement **promoPawns) {
    //----------------rysowanie mapy--------------------//
    rect->x = 0;
    rect->y = 0;
    SDL_BlitSurface(promoSurface, NULL, ekran, rect); //ustawienie pierwszego prostokata i wrzucenie go do okna

    //--------------rysowanie pionkow-------------------//
    for (int i = 0; i < 4; i++) {
        rect->x = promoOffsetX + (promoPawns[i]->col - 1) * fieldW;
        rect->y = promoOffsetY;
        SDL_BlitSurface(promoPawns[i]->pawnSurface, NULL, ekran, rect);
    }
}

void Promotion::freeSurface(PawnArrangement **promoPawns) {
    SDL_FreeSurface(promoSurface); //usuniecie obszaru mapy
    for (int i = 0; i < 4; i++) {
        SDL_FreeSurface(promoPawns[i]->pawnSurface);
    }
}

int Promotion::mouseBtnUp(int pressedX, int pressedY) {
    int col = Constants::clickOutOfPromoBox;
    if ((pressedX >= promoOffsetX) && (pressedX <= promoOffsetX + 4 * fieldW) && (pressedY >= promoOffsetY) && (pressedY <= promoOffsetY + fieldH)) {
        col = (pressedX - promoOffsetX) / fieldW;
        cout << "PROMOTION clicked: " << col << endl;
    }
    return col;
}




