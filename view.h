#ifndef VIEW
#define VIEW

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <sstream>
#include "constants.h" //stale
#include "pawn_arr.h" //struktury do udostepniania informacji o polozeniu pionkow
#include "pawn_move.h" //struktura do przesylania info o ruchu pionka

using namespace std;

//----------------------------------------------------------------//
//------------------------------WIDOK-----------------------------//
//----------------------------------------------------------------//


//-------------------WIDOK Glowny-------------------//
class View {
private:
    int mapOffsetX, mapOffsetY; //offset mapy od granicy okna
    int mapW, mapH; //wymiary mapy
    int fieldW, fieldH; //wymiary pojedynczego pola mapy
    SDL_Surface *mapSurface; //wskaznik do obiektu zawierajacego fotke mapy
    SDL_Surface *grabCoursor; //wskaznik do obiektu SDL grab pointer
    bool mouseButtonDownFlag; //czy klikniÍto przycisk myszki na mapie
    int row_clicked, col_clicked; //wsp tablicowe elementu na jakim zostal klikniety przycisk myszki
    int mouseMotionX, mouseMotionY; //wsp myszki podczas ruchu myszki z wcisnietym przyciskiem na jakims pionku
    friend class Controller;
public:
   View(int offX, int offY, int mapWidth, int mapHeight, int fieldWidth, int fieldHeight, SDL_Surface *surface);
   ~View();
   void renderView(SDL_Rect *rect, SDL_Surface *ekran, PawnArrangement ***pawns); //metoda renderujaca widok
   void freeSurface(PawnArrangement ***pawns); //metoda zwalniajaca pamiec RAM z juz nieaktualnych obiektow
   void mouseBtnDown(int pressedX, int pressedY); //metoda do przypisywania mouseButtonDownFlag = true, oraz wsp tablicowych kliknietego elementu
   PawnMove mouseBtnUp(int releasedX, int releasedY); //metoda do przypisywania mouseButtonDownFlag = false;
   void setMouseMotionCoord(int mouseX, int mouseY); //metoda do wpisywania wsp myszki podczas gdy jest wcisniety przycisk na jakiems pionku
};


//-------------------WIDOK Promocji-------------------//
class Promotion {
private:
    SDL_Surface *promoSurface; //wskaznik do obiektu zawierajacego obrazek tla promocji
    int promoOffsetX, promoOffsetY, fieldW, fieldH; //offset mapy od granicy okna
public:
    Promotion(int promoOffX = 200, int promoOffY = 350, int fWidth = 75, int fHeight = 75);
    ~Promotion();
    void renderView(SDL_Rect *rect, SDL_Surface *ekran, PawnArrangement **promoPawns); //metoda renderujaca widok
    void freeSurface(PawnArrangement **promoPawns); //metoda zwalniajaca pamiec RAM z juz nieaktualnych obiektow
    int mouseBtnUp(int pressedX, int pressedY); //metoda zwracajaca numer kliknietego pionka promocji
};



#endif // VIEW

