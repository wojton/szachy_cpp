#ifndef MODEL
#define MODEL

//----------------------------------------------------------------//
//------------------------------MODEL-----------------------------//
//----------------------------------------------------------------//

#include <iostream>
#include <sstream>
#include "constants.h" //stale
#include "model_innerclasses.h" //klasy wewnetrzne modelu
#include "model_struct.h" //struktury do udostepniania informacji o modelu
#include "pawn_arr.h" //struktury do udostepniania informacji o polozeniu pionkow
#include "pawn_move.h" //struktura do przesylania info o ruchu pionka
#include "frame_info.h" //struktura do przesylania info o akceptacji ruchu i zmianie frama
using namespace std;

class Model {
private:
    Map *mapp; //wskaznik do obiektu mapy
    Pawn ***pawnArr; //wskaznik do tablicy 64 wskaznikow na pionki
    PawnArrangement ***pawnsToDisplay; //wskaznik do 64 wskaznikow na info o pionkach -> dla widoku
    Pawn **promoPawnArr; //wskaznik do tablicy 4 wskaznikow na pionki z promocji
    PawnArrangement **promoPawnsToDisplay; //wskaznik do 4 wskaznikow na info o pionkach z promocji -> dla widoku
    int promoPawnRow, promoPawnCol; //wsp tablicowe pionka do promocji
public:
    Model();
    ~Model();
    void setPawnArr();
    void setPawnArrPointersNull();
    void setPromoPawnArr();
    void initMap();
    void initPawnArr();
    void initPawnsToDisplay();
    mapDet getMapViewDetails();
    PawnArrangement*** getPawnsToDisplayPointer();
    FrameInfo movePawn(PawnMove *pawnMoveDet, string moveTeam); //metoda sprawdzajaca ruch pionka / czy kliknieto w pionka wg / czy ruch jest zawodnikiem z odpowiedniego teamu
    void updatePawnPosition(PawnMove *pawnMoveDet); //uaktualnia pozycje pionka po ruchu tj przestawia pozycje wskaznika w tablicy + zmienia wartosci row i col w obiekcie pionka
    void createPromoPawns(int scene); //metoda do tworzenia obiektow pionkow do Promocji i struktur do info dla widoku
    bool changePromoPawn(int col); //metoda do zmieniania wybranego pionka z promocji
};

#endif // MODEL
