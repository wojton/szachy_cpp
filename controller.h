#ifndef CONTROLLER
#define CONTROLLER

//----------------------------------------------------------------//
//---------------------------CONTROLLER---------------------------//
//----------------------------------------------------------------//

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <windows.h> //do obslugi klawiatury
#include <sstream> //biblioteka do roznych konwersji int -> string np
#include "constants.h" //stale
#include "fps.cpp"
#include "model.h"
#include "model_struct.h"
#include "view.h"
#include "pawn_arr.h" //struktury do udostepniania informacji o polozeniu pionkow
#include "pawn_move.h" //struktura do przesylania info o ruchu pionka
#include "frame_info.h" //struktura do przesylania info o akceptacji ruchu i zmianie frama

using namespace std;


class Controller {
private:
    int frame, scene; //frame -> do zmian gry gdy np ktos wygra, scene -> do zmian gry przy wyborze pionka np
    string moveTeam; //team ktory teraz ma ruch
    Model *modelP;
    View *viewP;
    Promotion *promotion; //wskaznik do obiektu promocji
    SDL_Window *windowSDL; //okno do rysowania grafiki
    SDL_Surface *screenSDL;
    SDL_Event eventSDL; //obsluga zdarzen
    SDL_Rect rect1, rect2; //struktury bedace prostokatami
    Klasa_fps fps; //obiekt klasy fps
    PawnArrangement ***pawns; //wskaznik do struktury rozmieszczenia pionkow w modelu
    PawnArrangement **promoPawns; //wskaznik do struktury rozmieszczenia pionkow w promocji
public:
    Controller();
    ~Controller();
    void initSdl(); //inicjalizuje elementy grafiki SDL
    void initView(); //inicjalizuje obiekt widoku
    void gameFlow(); //petla w ktorej 1/60 sek renderowana jest mapa
};



#endif // CONTROLLER
