//----------------------------------------------------------------//
//---------------------------CONTROLLER---------------------------//
//----------------------------------------------------------------//

#include <iostream>
#include "controller.h"

using namespace std;

Controller::Controller() {
    frame = Constants::startFrame;
    scene = Constants::startScene;
    moveTeam = "white";
    modelP = new Model();
    pawns = modelP->getPawnsToDisplayPointer();
    promotion = NULL;
    this->initSdl();
    this->initView();
    this->gameFlow();
}

Controller::~Controller() {
    cout << "niszcze kontroler" << endl;
}

void Controller::initSdl() {
    SDL_Init(SDL_INIT_EVERYTHING);
    windowSDL = SDL_CreateWindow("Szachy", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1000, 700, NULL); //stworzenie okna
    screenSDL = SDL_GetWindowSurface(windowSDL); //grafika do okna -> stad okno wie skad ma czerpac obrazek
    TTF_Init();
}

void Controller::initView() {
    mapDet mvd = modelP->getMapViewDetails();
    viewP = new View(mvd.mapOffsetX, mvd.mapOffsetY, mvd.mapW, mvd.mapH, mvd.fieldW, mvd.fieldH, mvd.mapSurface);

    cout << "inicjalizacja gry -> tworze sie na koncu!!!" << endl;
    cout << "w kontrollerze filedH = " << mvd.fieldH << endl;
}



//-----------------------OBSLUGA GRY---------------------//
void Controller::gameFlow() {
    while(true) {
        if (frame == Constants::startFrame) {
            //----------------SCENA 1: Obsluga---------------//
            while (frame == Constants::startFrame) {
                //--------------kontrola fps na poczatek---------------//
                {
                    fps.start();
                }

                //------------------obsluga zdarzen--------------------//
                {
                    while (SDL_PollEvent(&eventSDL)) {
                        if (eventSDL.type == SDL_QUIT)
                        {
                            exit(0);
                        }
                        else if (eventSDL.type == SDL_MOUSEBUTTONDOWN)
                        {
                            if (scene == Constants::startScene) {
                                viewP->mouseBtnDown(eventSDL.button.x, eventSDL.button.y);
                                viewP->setMouseMotionCoord(eventSDL.button.x, eventSDL.button.y);
                                SDL_ShowCursor(0); // To disable it
                            }
                        }
                        else if (eventSDL.type == SDL_MOUSEBUTTONUP)
                        {
                            if (scene == Constants::startScene) {
                                SDL_ShowCursor(1);
                                PawnMove pawnMove = viewP->mouseBtnUp(eventSDL.button.x, eventSDL.button.y);
                                if (!(pawnMove.col == pawnMove.col_move && pawnMove.row == pawnMove.row_move)) {
                                    FrameInfo frameInfo;
                                    frameInfo = modelP->movePawn(&pawnMove, moveTeam); //odpalenie metody w modelu i wyslanie jej struktury z danymi o ruchu pionka
                                    //--------PRZYPISANIE TEAMU KTORY MA RUCH------//
                                    moveTeam = frameInfo.moveTeam; cout << "RUCH MA TERAZ: " << moveTeam << endl;
                                    cout << "tu kontroler -> dostalem info na ruch: " << frameInfo.correctMove << ", i frame: " << frameInfo.activeFrame << ", i scene: " << frameInfo.activeScene << endl;
                                    if (frameInfo.promoPawnsToDisplayI) {
                                        cout << "MAM INFO O PIONKACH DO PROMOCJI!!!" << endl;
                                        promotion = new Promotion(); //stworzenie obiektu promocji
                                        promoPawns = frameInfo.promoPawnsToDisplayI; //przypisanie wskaznika do pionkow z promocji
                                    }
                                    scene = frameInfo.activeScene;
                                }
                            } else if (scene == Constants::whitePromoScene || scene == Constants::blackPromoScene) {
                                int clickedCol = promotion->mouseBtnUp(eventSDL.button.x, eventSDL.button.y);
                                if (clickedCol != Constants::clickOutOfPromoBox) {
                                    modelP->changePromoPawn(clickedCol);
                                    scene = Constants::startScene;
                                    cout << "zmieniam znowu scene na 0" << endl;
                                }
                            }
                            cout << "----------------------------------------------------------------" << endl << endl;
                        }
                        else if (eventSDL.type == SDL_MOUSEMOTION)
                        {
                            if (scene == Constants::startScene) {
                                viewP->setMouseMotionCoord(eventSDL.motion.x, eventSDL.motion.y);
                            }
                        }
                    }
                }

                //----------------------render-------------------------//
                {
                    if (scene == Constants::startScene) {
                        viewP->renderView(&rect2, screenSDL, pawns);
                    } else if (scene == Constants::whitePromoScene) {
                        promotion->renderView(&rect2, screenSDL, promoPawns);
                    } else if (scene == Constants::blackPromoScene) {
                        promotion->renderView(&rect2, screenSDL, promoPawns);
                    }
                }

                //-----odswiezanie ekranu i kontrola fps na koniec-----//
                {
                    SDL_UpdateWindowSurface(windowSDL); //aktualizuje okno kopiujac ekran
                    fps.endd();
                }
            }

            //-------SCENA 1: Zwolnienie pamieci RAM---------//
            {
                if (scene == Constants::startScene) {
                    viewP->freeSurface(pawns);
                } else if (scene == Constants::whitePromoScene) {
                    promotion->freeSurface(promoPawns);
                } else if (scene == Constants::blackPromoScene) {
                    promotion->freeSurface(promoPawns);
                }
            }
        }

//        if (viewP->frame == 10) {
//            //--------------SCENA 10: Promocja bialego pionka------------//
//            while (viewP->frame == 10) {
//                //--------------kontrola fps na poczatek---------------//
//                {
//                    fps.start();
//                }
//
//                //------------------obsluga zdarzen--------------------//
//                {
//                    while (SDL_PollEvent(&eventSDL)) {
//                        if (eventSDL.type == SDL_QUIT)
//                        {
//                            exit(0);
//                        }
//                        else if (eventSDL.type == SDL_MOUSEBUTTONDOWN)
//                        {
//                            viewP->frame = 0;
//                        }
//                        else if (eventSDL.type == SDL_MOUSEBUTTONUP)
//                        {
//                            cout << "button up" << endl;
//                        }
//                        else if (eventSDL.type == SDL_MOUSEMOTION)
//                        {
//
//                        }
//                    }
//                }
//
//                //----------------------render-------------------------//
//                {
//                    rect2.x = 0;
//                    rect2.y = 0;
//                    SDL_BlitSurface(viewP->mapSurface, NULL, screenSDL, &rect2); //ustawienie pierwszego prostokata i wrzucenie go do okna
//                }
//
//                //-----odswiezanie ekranu i kontrola fps na koniec-----//
//                {
//                    SDL_UpdateWindowSurface(windowSDL); //aktualizuje okno kopiujac ekran
//                    fps.endd();
//                }
//            }
//
//
//            //-------SCENA 1: Zwolnienie pamieci RAM---------//
//            {
//                SDL_FreeSurface(viewP->mapSurface); //usuniecie obszaru mapy
//            }
//        }
    }
}
