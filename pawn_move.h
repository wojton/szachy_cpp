#ifndef PAWNMOVE
#define PAWNMOVE

#include <iostream>
using namespace std;

//----------struktura do przesylania info o ruchu pionka---------//

struct PawnMove {
    int row, col, row_move, col_move;
    PawnMove(int rowP, int colP, int rowmove, int colmove) {
        row = rowP;
        col = colP;
        row_move = rowmove;
        col_move = colmove;
    }
};

#endif // PAWNMOVE
