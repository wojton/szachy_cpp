#include <iostream>
#include "model.h"
using namespace std;

//----------------------------------------------------------------//
//------------------------------MODEL-----------------------------//
//----------------------------------------------------------------//

Model::Model() {
    setPawnArr();
    setPromoPawnArr();
    setPawnArrPointersNull();
    initMap();
    initPawnArr();
    initPawnsToDisplay();
    cout << "tworze obiekt modelu" << endl;
}

Model::~Model() {
    cout << "niszcze model" << endl;
}

void Model::setPawnArr() {
    pawnArr = new Pawn **[8]; // tablica zawieracajca 64 wskazniki na typ Pawn
    pawnsToDisplay = new PawnArrangement **[8];
    for (int loop = 0; loop < 8; loop++) {
        *(pawnArr + loop) = new Pawn *[8];
        *(pawnsToDisplay + loop) = new PawnArrangement *[8];
    }
}

void Model::setPromoPawnArr() {
    promoPawnArr = new Pawn *[4];
    promoPawnsToDisplay = new PawnArrangement *[4];
    for (int i = 0; i < 4; i++) {
        promoPawnArr[i] = NULL;
        promoPawnsToDisplay[i] = NULL;
    }
}

void Model::setPawnArrPointersNull() {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            pawnArr[i][j] = NULL;
            pawnsToDisplay[i][j] = NULL;
        }
    }
}

void Model::initMap() {
    mapp = new Map("img/mapka.png");
}

void Model::initPawnArr() {
    //-------white team-----------//
    pawnArr[0][0] = new Rook(0, 0, "white", "white_rook1", "img/white_wieza.png");
    pawnArr[0][7] = new Rook(0, 7, "white", "white_rook2", "img/white_wieza.png");
    pawnArr[0][1] = new Bishop(0, 1, "white", "white_goniec1", "img/white_goniec.png");
    pawnArr[0][6] = new Bishop(0, 6, "white", "white_goniec2", "img/white_goniec.png");
    pawnArr[0][2] = new Knight(0, 2, "white", "white_skoczek1", "img/white_skoczek.png");
    pawnArr[0][5] = new Knight(0, 5, "white", "white_skoczek2", "img/white_skoczek.png");
    pawnArr[0][3] = new Queen(0, 3, "white", "white_hetman", "img/white_hetman.png");
    pawnArr[0][4] = new King(0, 4, "white", "white_krol", "img/white_krol.png");
    pawnArr[1][0] = new PPawn(1, 0, "white", "white_pionek", "img/white_pionek.png");

    //-------black team-----------//
    pawnArr[7][0] = new Rook(7, 0, "black", "black_rook1", "img/black_wieza.png");
    pawnArr[7][7] = new Rook(7, 7, "black", "black_rook2", "img/black_wieza.png");
    pawnArr[7][1] = new Bishop(7, 1, "black", "black_goniec1", "img/black_goniec.png");
    pawnArr[7][6] = new Bishop(7, 6, "black", "black_goniec2", "img/black_goniec.png");
    pawnArr[7][2] = new Knight(7, 2, "black", "black_skoczek1", "img/black_skoczek.png");
    pawnArr[7][5] = new Knight(7, 5, "black", "black_skoczek2", "img/black_skoczek.png");
    pawnArr[7][3] = new Queen(7, 3, "black", "black_hetman", "img/black_hetman.png");
    pawnArr[7][4] = new King(7, 4, "black", "black_krol", "img/black_krol.png");
    pawnArr[6][0] = new PPawn(6, 0, "black", "black_pionek", "img/black_pionek.png");
}

void Model::initPawnsToDisplay() {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (pawnArr[i][j]) {
                pawnsToDisplay[i][j] = pawnArr[i][j]->getPawnDet();
            }
        }
    }
}

mapDet Model::getMapViewDetails() {
    mapDet mapdet;
    mapdet.mapOffsetX = mapp->mapOffsetX;
    mapdet.mapOffsetY = mapp->mapOffsetY;
    mapdet.mapH = mapp->mapH;
    mapdet.mapW = mapp->mapW;
    mapdet.fieldW = mapp->fieldW;
    mapdet.fieldH = mapp->fieldH;
    mapdet.mapSurface = mapp->mapSurface;
    return mapdet;
}

PawnArrangement*** Model::getPawnsToDisplayPointer() {
    return pawnsToDisplay;
}

FrameInfo Model::movePawn(PawnMove *pawnMoveDet, string moveTeam) {
    FrameInfo frameInfo;
    if (pawnArr[pawnMoveDet->row][pawnMoveDet->col]) { //jesli zlapiemy pionka to zostanie sprawdzony ruch -> jesli puste pole nie wykona sie
        if (pawnArr[pawnMoveDet->row][pawnMoveDet->col]->getPawnTeam() == moveTeam) {
            frameInfo = pawnArr[pawnMoveDet->row][pawnMoveDet->col]->checkPawnMove(pawnMoveDet->row_move, pawnMoveDet->col_move, pawnArr); //odpalenie metody sprawdzajacej ruch w obiekcie pionka stojacego w danym miejscu
            cout << "odpowiedz modelu na ruch pionka: " << frameInfo.correctMove << endl;

            //-------------POPRAWNY ruch pionka------------//
            if(frameInfo.correctMove) {
                //--------PRZYPISANIE TEAMU KTORY MA RUCH------//
                frameInfo.moveTeam = (moveTeam == "white") ? "black" : "white";
                //----------AKTUALIZACJA POZYCJI PIONKA--------//
                this->updatePawnPosition(pawnMoveDet);
                cout << "zmiana miejsc" << endl;
            } else {
                 //--------PRZYPISANIE TEAMU KTORY MA RUCH------//
                frameInfo.moveTeam = moveTeam;
            }
            //-----------PROMOCJA bialego pionka----------//
            if (frameInfo.activeScene == Constants::whitePromoScene) {
                promoPawnRow = pawnMoveDet->row_move;
                promoPawnCol = pawnMoveDet->col_move;
                this->createPromoPawns(Constants::whitePromoScene);
                frameInfo.promoPawnsToDisplayI = promoPawnsToDisplay;
            }

            //-----------PROMOCJA czarnego pionka----------//
            if (frameInfo.activeScene == Constants::blackPromoScene) {
                promoPawnRow = pawnMoveDet->row_move;
                promoPawnCol = pawnMoveDet->col_move;
                this->createPromoPawns(Constants::blackPromoScene);
                frameInfo.promoPawnsToDisplayI = promoPawnsToDisplay;
            }
        } else {
            frameInfo.moveTeam = moveTeam;
        }
    }
    return frameInfo;
}

void Model::updatePawnPosition(PawnMove *pawnMoveDet) {
    //usuniecie obiektu pionka
    if (pawnArr[pawnMoveDet->row_move][pawnMoveDet->col_move]) {
        delete pawnArr[pawnMoveDet->row_move][pawnMoveDet->col_move];
    }
    //przestawienie wskaznikow
    pawnArr[pawnMoveDet->row_move][pawnMoveDet->col_move] = pawnArr[pawnMoveDet->row][pawnMoveDet->col];
    pawnsToDisplay[pawnMoveDet->row_move][pawnMoveDet->col_move] = pawnsToDisplay[pawnMoveDet->row][pawnMoveDet->col];
    pawnArr[pawnMoveDet->row][pawnMoveDet->col] = NULL;
    pawnsToDisplay[pawnMoveDet->row][pawnMoveDet->col] = NULL;
    //przestawianie row i col w obiekcie pionka
    pawnArr[pawnMoveDet->row_move][pawnMoveDet->col_move]->updatePawnPosition(pawnMoveDet->row_move, pawnMoveDet->col_move);
    pawnsToDisplay[pawnMoveDet->row_move][pawnMoveDet->col_move]->row = pawnMoveDet->row_move;
    pawnsToDisplay[pawnMoveDet->row_move][pawnMoveDet->col_move]->col = pawnMoveDet->col_move;
}

void Model::createPromoPawns(int scene) {
    if (scene == Constants::whitePromoScene) {
        promoPawnArr[0] = new Rook(0, 1, "white", "white_rook_promo", "img/white_wieza.png");
        promoPawnArr[1] = new Bishop(0, 2, "white", "white_goniec_promo", "img/white_goniec.png");
        promoPawnArr[2] = new Knight(0, 3, "white", "white_skoczek_promo", "img/white_skoczek.png");
        promoPawnArr[3] = new Queen(0, 4, "white", "white_hetman_promo", "img/white_hetman.png");
    } else if (scene == Constants::blackPromoScene) {
        promoPawnArr[0] = new Rook(0, 1, "black", "black_rook_promo", "img/black_wieza.png");
        promoPawnArr[1] = new Bishop(0, 2, "black", "black_goniec_promo", "img/black_goniec.png");
        promoPawnArr[2] = new Knight(0, 3, "black", "black_skoczek_promo", "img/black_skoczek.png");
        promoPawnArr[3] = new Queen(0, 4, "black", "black_hetman_promo", "img/black_hetman.png");
    }
    for (int i = 0; i < 4; i++) {
        promoPawnsToDisplay[i] = promoPawnArr[i]->getPawnDet();
    }
}

bool Model::changePromoPawn(int col) {
    cout << "PROMOCJA PIONKA o wsp [" << promoPawnRow << ", " << promoPawnCol << "] -> " << col << endl;

    //------update pionka dla modelu--------//
    promoPawnArr[col]->updatePawnPosition(promoPawnRow, promoPawnCol); //zmiana wspolrzednych wybranego pionka
    delete pawnArr[promoPawnRow][promoPawnCol];
    pawnArr[promoPawnRow][promoPawnCol] = promoPawnArr[col];

    //-------update pionka dla widou-------//
    promoPawnsToDisplay[col]->row = promoPawnRow;
    promoPawnsToDisplay[col]->col = promoPawnCol;
    delete pawnsToDisplay[promoPawnRow][promoPawnCol];
    pawnsToDisplay[promoPawnRow][promoPawnCol] = promoPawnsToDisplay[col];

    for (int i = 0; i < 4; i++) {
        if (i != col) {
                delete promoPawnArr[i];
                delete promoPawnsToDisplay[i];
        } //usuniecie obiektu pionka w tablicy promocji
        promoPawnArr[i] = NULL; //ustawienie wskaznika na Null
        promoPawnsToDisplay[i] = NULL;
    }
    return true;
}
