#ifndef PAWNARR
#define PAWNARR

#include <iostream>
using namespace std;

//----------struktura do przesylania informacji o tablicy pionkow----------//

struct PawnArrangement {
    int row, col;
    SDL_Surface *pawnSurface;
    PawnArrangement(int rowP, int colP, SDL_Surface *surface){
        row = rowP;
        col = colP;
        pawnSurface = surface;
    }
};

#endif // PAWNARR
