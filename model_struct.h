#ifndef MODELSTRUCT
#define MODELSTRUCT

#include <iostream>
using namespace std;

//----------struktura do przesylania informacji o mapie----------//
struct mapDet {
    int mapOffsetX, mapOffsetY; //offset mapy od granicy okna
    int mapW, mapH; //wymiary mapy
    int fieldW, fieldH; //wymiary pojedynczego pola mapy
    SDL_Surface *mapSurface; //wskaznik do obiektu zawierajacego fotke mapy
};

#endif // MODELSTRUCT
