#ifndef FRAMEINFO
#define FRAMEINFO

#include <iostream>
#include "pawn_arr.h" //struktury do udostepniania informacji o polozeniu pionkow
using namespace std;

//----------struktura do przesylania informacji o zmianach frame, scene, czy poprawny ruch----------//

struct FrameInfo {
    bool correctMove;
    int activeFrame, activeScene;
    int rowOppCollision, colOppCollision; //wsp pionka ktory spowodowal ze meotdy sprawdzajace ruch zwrocily blad ruchu
    string moveTeam; //info o teamie ktory ma teraz ruch
    PawnArrangement **promoPawnsToDisplayI; //wskaznik do tablicy wskaznikow do struktur z pionkami do Promocji -> info dla widoku
    FrameInfo(bool correctM = false, int activeF = 0, int activeS = 0, PawnArrangement **promoPawnsI = NULL){
        correctMove = correctM;
        activeFrame = activeF;
        activeScene = activeS;
        promoPawnsToDisplayI = promoPawnsI;
    }
};

#endif // FRAMEINFO
