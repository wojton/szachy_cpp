#ifndef MODELINNERCLASSES
#define MODELINNERCLASSES

//----------------------------------------------------------------//
//------------------------------MODEL-----------------------------//
//----------------------------------------------------------------//

#include <iostream>
#include <sstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include "constants.h" //stale
#include "pawn_arr.h" //struktury do udostepniania informacji o polozeniu pionkow
#include "frame_info.h" //struktura do przesylania info o akceptacji ruchu i zmianie frama
using namespace std;

//---------------------------KLASA MAPY---------------------------//
class Map {
private:
    int mapOffsetX, mapOffsetY; //offset mapy od granicy okna
    int mapW, mapH; //wymiary mapy
    int fieldW, fieldH; //wymiary pojedynczego pola mapy
    SDL_Surface *mapSurface; //wskaznik do obiektu zawierajacego fotke mapy
public:
    Map(string urlM, int offX = 50, int offY = 50, int mapWidth = 600, int mapHeight = 600, int fieldWidth = 75, int fieldHeight = 75);
    ~Map();
    friend class Model;
};

//---------------------KLASA WIRTUALNA PIONKA---------------------//
class Pawn {
protected:
    int row, col;
    string team;
    string name;
    SDL_Surface *pawnSurface; //wskaznik do obiektu zawierajacego fotke pionka
public:
    //---------- metoda sprawdzajaca ruch danego pionka->jesli sprawdza dla ruchu krola to kingBeatingPos = true ------//
    virtual FrameInfo checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos = false) = 0;
    virtual ~Pawn(){};
    string getPawnTeam();
    string getPawnName();
    PawnArrangement* getPawnDet(); //informacje o pionku tj row, col, surface zwraca wskaznik do struktury z takimi info
    void updatePawnPosition(int row_move, int col_move); //metoda do aktualizowania wsp ro, col pionka po ruchu
};


//----------------KLASA DO SPRAWDZANIA RUCHU PIONKA---------------//
class CheckMove {
public:
    static bool teamMate(int row_move, int col_move, Pawn ***pawnArr, string team); //sprawdzenie czy na wskazanym miejscu stoi gracz z naszej druzyny
    static FrameInfo rook(int row, int col, int row_move, int col_move, Pawn ***pawnArr); //sprawdzenie czy jakis pionek stoi na drodze dla wiezy
    static FrameInfo bishop(int row, int col, int row_move, int col_move, int diffRowMod, Pawn ***pawnArr); //sprawdzenie czy jakis pionek stoi na drodze dla gonca
    static bool whitePawn(int row, int col, int row_move, int col_move, Pawn ***pawnArr);
    static bool blackPawn(int row, int col, int row_move, int col_move, Pawn ***pawnArr);
    static bool kingBeatingPos(int row_move, int col_move, Pawn ***pawnArr, string team, bool exposeKingPos = false, int row = 0, int col = 0); //sprawdzenie czy po ruchu krola nie bedzie on pod biciem przeciwnika + wykorzystane takze dla metody exposeKingPos wtedy dodatkowe parametry row i col -> jesli zwrotka z metody pionka przyjdzie false ale row i col takie same jak pionka co sie rusza to jednak true
    static bool exposeKingPos(Pawn ***pawnArr, string team, int row, int col); //sprawdzenie czy po ruchu pionka nie wystawimy krola na bicie
};

//--------------------------KLASA WIEZA---------------------------//
class Rook :public Pawn {
public:
    Rook(int rowP, int colP, string teamP, string nameP, string urlP);
    ~Rook();
    virtual FrameInfo checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos = false);
};


//--------------------------KLASA GONIEC--------------------------//
class Bishop :public Pawn {
public:
    Bishop(int rowP, int colP, string teamP, string nameP, string urlP);
    ~Bishop();
    virtual FrameInfo checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos = false);
};


//--------------------------KLASA HETMAN--------------------------//
class Queen :public Pawn {
public:
    Queen(int rowP, int colP, string teamP, string nameP, string urlP);
    ~Queen();
    virtual FrameInfo checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos = false);
};


//-------------------------KLASA SKOCZEK--------------------------//
class Knight :public Pawn {
public:
    Knight(int rowP, int colP, string teamP, string nameP, string urlP);
    ~Knight();
    virtual FrameInfo checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos = false);
};

//-------------------------KLASA PIONEK--------------------------//
class PPawn :public Pawn {
public:
    PPawn(int rowP, int colP, string teamP, string nameP, string urlP);
    ~PPawn();
    virtual FrameInfo checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos = false);
};

//--------------------------KLASA KROL--------------------------//
class King :public Pawn {
public:
    King(int rowP, int colP, string teamP, string nameP, string urlP);
    ~King();
    virtual FrameInfo checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos = false);
};

#endif // MODELINNERCLASSES
