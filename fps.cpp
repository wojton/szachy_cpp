#include <time.h>
#include <SDL2/SDL.h>

using namespace std;

class Klasa_fps {
public:
    int fps_start;
    int fps_end;
    int difference;
    float delay;
    int fps = 30;

    void start() {
        fps_start = clock();
    }

    void endd() {
        fps_end = clock();
        difference = fps_end - fps_start;
        delay = (1000.0 / fps) - difference;
        if (delay > 0) {
            SDL_Delay(delay);
        }
    }
};
