#include <iostream>
#include "model_innerclasses.h"
using namespace std;

//----------------------------------------------------------------//
//------------------------------MODEL-----------------------------//
//----------------------------------------------------------------//


//---------------------------KLASA MAPY---------------------------//

Map::Map(string urlM, int offX, int offY, int mapWidth, int mapHeight, int fieldWidth, int fieldHeight) {
    mapOffsetX = offX;
    mapOffsetY = offY;
    mapW = mapWidth;
    mapH = mapHeight;
    fieldW = fieldWidth;
    fieldH = fieldHeight;
    mapSurface = IMG_Load(urlM.c_str());
    cout << "tworze obiekt mapy, fieldH = "<< fieldHeight << endl;
}

Map::~Map() {
    cout << "niszcze obiekt mapy" << endl;
}


//--------------KLASA DO SPRAWDZANIA RUCHU PIONKA-----------------//

bool CheckMove::teamMate(int row_move, int col_move, Pawn ***pawnArr, string team) {
    if (pawnArr[row_move][col_move]) { //jesli na wskazanym miejscu stoi gracz z naszej druzyny
        if (pawnArr[row_move][col_move]->getPawnTeam() == team) {
            return false;
        }
    }
    return true;
}

FrameInfo CheckMove::rook(int row, int col, int row_move, int col_move, Pawn ***pawnArr) {
    FrameInfo frameInfo(false);
    if (col_move > (col + 1)) { //zmiana po wierszu na prawo (+ 1 bo jak obok to na pewno moze sie tam ruszyc)
        for (int i = (col + 1); i < col_move; i++) {
            cout << "sprawdzam pozycje: [" << row << ", " << i << "]" << endl;
            if (pawnArr[row][i]) {
                frameInfo.rowOppCollision = row;
                frameInfo.colOppCollision = i;
                return frameInfo;
            }
        }
    } else if (col_move < (col - 1)) {
        for (int i = (col - 1); i > col_move; i--) {
            cout << "sprawdzam pozycje: [" << row << ", " << i << "]" << endl;
            if (pawnArr[row][i]) {
                frameInfo.rowOppCollision = row;
                frameInfo.colOppCollision = i;
                return frameInfo;
            }
        }
    } else if (row_move > (row + 1)) {
        for (int i = (row + 1); i < row_move; i++) {
            cout << "sprawdzam pozycje: [" << i << ", " << col << "]" << endl;
            if (pawnArr[i][col]) {
                frameInfo.rowOppCollision = i;
                frameInfo.colOppCollision = col;
                return frameInfo;
            }
        }
    } else if (row_move < (row - 1)) {
        for (int i = (row - 1); i > row_move; i--) {
            cout << "sprawdzam pozycje: [" << i << ", " << col << "]" << endl;
            if (pawnArr[i][col]) {
                frameInfo.rowOppCollision = i;
                frameInfo.colOppCollision = col;
                return frameInfo;
            }
        }
    }
    frameInfo.correctMove = true;
    return frameInfo;
}

FrameInfo CheckMove::bishop(int row, int col, int row_move, int col_move, int diffRowMod, Pawn ***pawnArr) {
    FrameInfo frameInfo(false);
    int diffRow = row_move - row;
    int diffCol = col_move - col;
    for (int i = 1; i < diffRowMod; i++) {
        int rowTemp, colTemp;
        if (diffRow > 1 && diffCol > 1) { rowTemp = row + i; colTemp = col + i; }
        else if (diffRow < -1 && diffCol < -1) { rowTemp = row - i; colTemp = col - i; }
        else if (diffRow > 1 && diffCol < -1) { rowTemp = row + i; colTemp = col - i; }
        else if (diffRow < -1 && diffCol > 1) { rowTemp = row - i; colTemp = col + i; }

        if (pawnArr[rowTemp][colTemp]) {
            frameInfo.rowOppCollision = rowTemp;
            frameInfo.colOppCollision = colTemp;
            return frameInfo;
        }
        cout << "sprawdzam pozycje: [" << rowTemp << ", " << colTemp << "]" << endl;
    }
    frameInfo.correctMove = true;
    return frameInfo;
}

bool CheckMove::whitePawn(int row, int col, int row_move, int col_move, Pawn ***pawnArr) {
    if (row_move <= row) { return false; }
    else if (row == 1 && (row_move == row + 2) && col_move == col) { //szczegolny przypadek pionka bialego jak stoi na 1 linii
        if (pawnArr[row + 1][col] || pawnArr[row + 2][col]) { return false; }
        else { return true; }
    } else {
        if (row_move == (row + 1) && col_move == col && (!pawnArr[row_move][col_move])) { return true; }
        if (row_move == (row + 1) && (col_move == (col + 1) || col_move == (col - 1)) && pawnArr[row_move][col_move]) {
            if (pawnArr[row_move][col_move]->getPawnTeam() == "black") { return true; }
        }
    }
    return false;
}

bool CheckMove::blackPawn(int row, int col, int row_move, int col_move, Pawn ***pawnArr) {
    if (row_move >= row) { return false; }
    else if (row == 6 && (row_move == row - 2) && col_move == col) { //szczegolny przypadek pionka czarnego jak stoi na 6 linii
        if (pawnArr[row - 1][col] || pawnArr[row - 2][col]) { return false; }
        else { return true; }
    } else {
        if (row_move == (row - 1) && col_move == col && (!pawnArr[row_move][col_move])) { return true; }
        if (row_move == (row - 1) && (col_move == (col + 1) || col_move == (col - 1)) && pawnArr[row_move][col_move]) {
            if (pawnArr[row_move][col_move]->getPawnTeam() == "white") { return true; }
        }
    }
    return false;
}


//------------sprawdzenie czy po ruchu krola nie bedzie on pod biciem przeciwnika-----------//
bool CheckMove::kingBeatingPos(int row_move, int col_move, Pawn ***pawnArr, string team, bool exposeKingPos, int row, int col) {
    string opponentTeam = (team == "white") ? "black" : "white";
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (pawnArr[i][j]) {
                if ((pawnArr[i][j]->getPawnTeam() == opponentTeam) && (pawnArr[i][j]->getPawnName() != "white_krol") && (pawnArr[i][j]->getPawnName() != "black_krol")) {
                    FrameInfo correctMoveInfo = pawnArr[i][j]->checkPawnMove(row_move, col_move, pawnArr, true);
                    if (correctMoveInfo.correctMove && !exposeKingPos) {
                        cout << "bicie na krola pionkiem: " << pawnArr[i][j]->getPawnName() << endl;
                        return false;
                    } else if (!correctMoveInfo.correctMove && exposeKingPos && correctMoveInfo.rowOppCollision == row && correctMoveInfo.colOppCollision == col) {
                        cout << "bicie na krola pionkiem: " << pawnArr[i][j]->getPawnName() << " -> ominieto spr zawodnika na poz ["<< row << ", "<< col << "]" << endl;
                        return false;
                    }
                }
            }
        }
    }
    return true;
}


//-----------sprawdzenie czy po ruchu pionka nie wystawimy krola na bicie----------//
bool CheckMove::exposeKingPos(Pawn ***pawnArr, string team, int row, int col) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (pawnArr[i][j]) {
                if ((pawnArr[i][j]->getPawnTeam() == team) && ((pawnArr[i][j]->getPawnName() == "white_krol") || (pawnArr[i][j]->getPawnName() == "black_krol"))) {
                    if (!CheckMove::kingBeatingPos(i, j, pawnArr, team, true, row, col)) {
                        cout << "po ruchu zawodnika z druzyny: " << team << ", KROL na pozycji [" << i << ", " << j << "] bedzie wystawiony na bicie!" << endl;
                        return false;
                    } else {
                        cout << "po ruchu zawodnika z druzyny: " << team << ", KROL na pozycji [" << i << ", " << j << "] jest czysty!" << endl;
                        return true;
                    }
                }
            }
        }
    }
}



//------------------METODY WIRTUALNEJ KLASY PAWN------------------//

string Pawn::getPawnTeam() {
    return team;
}

string Pawn::getPawnName() {
    return name;
}

PawnArrangement* Pawn::getPawnDet() {
    PawnArrangement *pawnArr = new PawnArrangement(row, col, pawnSurface);
    return pawnArr;
}

void Pawn::updatePawnPosition(int row_move, int col_move) {
    row = row_move;
    col = col_move;
}



//---------------------------KLASA WIEZA--------------------------//

Rook::Rook(int rowP, int colP, string teamP, string nameP, string urlP) {
    row = rowP;
    col = colP;
    team = teamP;
    name = nameP;
    pawnSurface = IMG_Load(urlP.c_str());
}

Rook::~Rook() {
    cout << "obiekt: -- " << name << " -- o wsp -> [" << this->row << ", " << this->col << "] -> usuniety" << endl;
}

FrameInfo Rook::checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos) {
    FrameInfo frameInfo(false);
    if ((row_move != row) && (col_move != col)) { return frameInfo; } //jesli ruch jest inny niz po wierszu albo kolomnie

    FrameInfo oppCollision = CheckMove::rook(row, col, row_move, col_move, pawnArr); //sprawdzenie innych pionkow na drodze
    if (!oppCollision.correctMove) { return oppCollision; }
    if (!kingBeatingPos) { //jesli sprawdzanie do ruchu krola to nie sprawdza czy na danej pozycji jest kolega z druzyny + czy po tym ewentualnym ruchu odslonimy krola
        if (!CheckMove::teamMate(row_move, col_move, pawnArr, team)) { return frameInfo; }
        if (!CheckMove::exposeKingPos(pawnArr, team, row, col)) { return frameInfo; }
    }
    frameInfo.correctMove = true;
    return frameInfo;
}


//---------------------------KLASA GONIEC--------------------------//

Bishop::Bishop(int rowP, int colP, string teamP, string nameP, string urlP) {
    row = rowP;
    col = colP;
    team = teamP;
    name = nameP;
    pawnSurface = IMG_Load(urlP.c_str());
}

Bishop::~Bishop() {
    cout << "obiekt: -- " << name << " -- o wsp -> [" << this->row << ", " << this->col << "] -> usuniety" << endl;
}

FrameInfo Bishop::checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos) {
    FrameInfo frameInfo(false);
    int diffRowMod = (row_move - row) > 0 ? row_move - row : row - row_move;
    int diffColMod = (col_move - col) > 0 ? col_move - col : col - col_move;
    if (diffRowMod != diffColMod) { return frameInfo; } //jesli ruch jest inny niz po wierszu albo kolumnie

    FrameInfo oppCollision = CheckMove::bishop(row, col, row_move, col_move, diffRowMod, pawnArr); //sprawdzenie innych pionkow na drodze
    if (!oppCollision.correctMove) { return oppCollision; }

    if (!kingBeatingPos) { //jesli sprawdzanie do ruchu krola to nie sprawdza czy na danej pozycji jest kolega z druzyny + czy po tym ewentualnym ruchu odslonimy krola
        if (!CheckMove::teamMate(row_move, col_move, pawnArr, team)) { return frameInfo; }
        if (!CheckMove::exposeKingPos(pawnArr, team, row, col)) { return frameInfo; }
    }
    frameInfo.correctMove = true;
    return frameInfo;
}


//---------------------------KLASA HETMAN---------------------------//

Queen::Queen(int rowP, int colP, string teamP, string nameP, string urlP) {
    row = rowP;
    col = colP;
    team = teamP;
    name = nameP;
    pawnSurface = IMG_Load(urlP.c_str());
}

Queen::~Queen() {
    cout << "obiekt: -- " << name << " -- o wsp -> [" << this->row << ", " << this->col << "] -> usuniety" << endl;
}

FrameInfo Queen::checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos) {
    FrameInfo frameInfo;
    int diffRowMod = (row_move - row) > 0 ? row_move - row : row - row_move;
    int diffColMod = (col_move - col) > 0 ? col_move - col : col - col_move;
    if (!((diffRowMod == diffColMod) || (row_move == row) || (col_move == col))) { return frameInfo; } //jesli ruch jest inny niz po wierszu albo kolumnie
    if (diffRowMod == diffColMod) { //ruch jak goniec
        FrameInfo oppCollision = CheckMove::bishop(row, col, row_move, col_move, diffRowMod, pawnArr); //sprawdzenie innych pionkow na drodze
        if (!oppCollision.correctMove) { return oppCollision; }
    } else if (diffRowMod == 0 || diffColMod == 0) { //ruch jak wieza
        FrameInfo oppCollision = CheckMove::rook(row, col, row_move, col_move, pawnArr); //sprawdzenie innych pionkow na drodze
        if (!oppCollision.correctMove) { return oppCollision; }
    }

    if (!kingBeatingPos) { //jesli sprawdzanie do ruchu krola to nie sprawdza czy na danej pozycji jest kolega z druzyny + czy po tym ewentualnym ruchu odslonimy krola
        if (!CheckMove::teamMate(row_move, col_move, pawnArr, team)) { return frameInfo; }
        if (!CheckMove::exposeKingPos(pawnArr, team, row, col)) { return frameInfo; }
    }

    frameInfo.correctMove = true;
    return frameInfo;
}

//---------------------------KLASA SKOCZEK--------------------------//

Knight::Knight(int rowP, int colP, string teamP, string nameP, string urlP) {
    row = rowP;
    col = colP;
    team = teamP;
    name = nameP;
    pawnSurface = IMG_Load(urlP.c_str());
}

Knight::~Knight() {
    cout << "obiekt: -- " << name << " -- o wsp -> [" << this->row << ", " << this->col << "] -> usuniety" << endl;
}

FrameInfo Knight::checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos) {
    FrameInfo frameInfo;
    int diffRowMod = (row_move - row) > 0 ? row_move - row : row - row_move;
    int diffColMod = (col_move - col) > 0 ? col_move - col : col - col_move;
    if (!((diffRowMod == 1 && diffColMod == 2) || (diffRowMod == 2 && diffColMod == 1))) { return frameInfo; }

    if (!kingBeatingPos) { //jesli sprawdzanie do ruchu krola to nie sprawdza czy na danej pozycji jest kolega z druzyny + czy po tym ewentualnym ruchu odslonimy krola
        if (!CheckMove::teamMate(row_move, col_move, pawnArr, team)) { return frameInfo; }
        if (!CheckMove::exposeKingPos(pawnArr, team, row, col)) { return frameInfo; }
    }
    frameInfo.correctMove = true;
    return frameInfo;
}


//---------------------------KLASA PIONEK--------------------------//

PPawn::PPawn(int rowP, int colP, string teamP, string nameP, string urlP) {
    row = rowP;
    col = colP;
    team = teamP;
    name = nameP;
    pawnSurface = IMG_Load(urlP.c_str());
}

PPawn::~PPawn() {
    cout << "obiekt: -- " << name << " -- o wsp -> [" << this->row << ", " << this->col << "] -> usuniety" << endl;
}

FrameInfo PPawn::checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos) {
    FrameInfo frameInfo;
    int diffRowMod = (row_move - row) > 0 ? row_move - row : row - row_move;
    int diffColMod = (col_move - col) > 0 ? col_move - col : col - col_move;

    if (!kingBeatingPos) { //jesli sprawdzanie do ruchu krola to nie sprawdza czy na danej pozycji jest kolega z druzyny + czy po tym ewentualnym ruchu odslonimy krola
        if (!CheckMove::teamMate(row_move, col_move, pawnArr, team)) { return frameInfo; }
        if (!CheckMove::exposeKingPos(pawnArr, team, row, col)) { return frameInfo; }
    }

    if (this->team == "white") {
        if (!CheckMove::whitePawn(row, col, row_move, col_move, pawnArr)) { return frameInfo; } //sprawdzenie ruchu dla bialego pionka
        if (row_move == 7) {
            cout << "ZMIANA BIALEGO PIONKA NA COSIK INNEGO" << endl;
            frameInfo.activeScene = Constants::whitePromoScene;
        }
    } else if (this->team == "black") {
        if (!CheckMove::blackPawn(row, col, row_move, col_move, pawnArr)) { return frameInfo; } //sprawdzenie ruchu dla czarnego pionka
        if (row_move == 0) {
            cout << "ZMIANA CZARNEGO PIONKA NA COSIK INNEGO" << endl;
            frameInfo.activeScene = Constants::blackPromoScene;
        }
    }

    frameInfo.correctMove = true;
    cout << frameInfo.correctMove << " , " << frameInfo.activeFrame << endl;
    return frameInfo;
}


//----------------------------KLASA KROL--------------------------//

King::King(int rowP, int colP, string teamP, string nameP, string urlP) {
    row = rowP;
    col = colP;
    team = teamP;
    name = nameP;
    pawnSurface = IMG_Load(urlP.c_str());
}

King::~King() {
    cout << "obiekt: -- " << name << " -- o wsp -> [" << this->row << ", " << this->col << "] -> usuniety" << endl;
}

FrameInfo King::checkPawnMove(int row_move, int col_move, Pawn ***pawnArr, bool kingBeatingPos) {
    FrameInfo frameInfo;
    Pawn *that = pawnArr[row_move][col_move];
    int diffRowMod = (row_move - row) > 0 ? row_move - row : row - row_move;
    int diffColMod = (col_move - col) > 0 ? col_move - col : col - col_move;

    if (!CheckMove::teamMate(row_move, col_move, pawnArr, team)) { return frameInfo; }
    if (!((diffRowMod == 1 || diffRowMod == 0) && (diffColMod == 1 || diffColMod == 0))) { return frameInfo; }
    if (that) { //sprawdzenie czy nie bije drugiego krola
        if (that->getPawnName() == "white_krol" || that->getPawnName() == "black_krol") { return frameInfo; }
    }

    //---------sprawdzenie czy na polu do ruchu jest mozliwe bicie jakiegos przeciwnika-----------//
    if (!CheckMove::kingBeatingPos(row_move, col_move, pawnArr, team)) { return frameInfo; }

    frameInfo.correctMove = true;
    return frameInfo;
}
